<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Network Engineer </title>
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/network-architect"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Network Engineer</h1>
                    <div class="sub-title text-center white-color"> United States (Remote)</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <p> <span class="txt_clr"><strong>Designation</strong> : </span>  Network Engineer</p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry</p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span>United States (Remote)  </p>
                        <p> <span class="txt_clr"><strong>Job Description</strong></span></p>
                        <ol>
                        <li>Hands on experience on Cisco routers, switches and firewall vendors like Palo alto, Checkpoint, Juniper. Cisco ASA, CISCO ISE and Load balancers A10 and F5, Cisco. </li>
<li>Must have Juniper Firewall experience. </li>
<li>Routing and Switching, TCP/IP, PPP, Ethernet, Fast Ethernet, FTP, DHCP, OSPF, VLAN, VTP, BGP, EIGRP, ACL, VoIP, Firewall Configuration. </li>
<li>Must have SD WAN Versa/viptela experience. </li>
<li>Configuration and troubleshooting. </li>
<li>Creating and monitoring new internal and external policies. </li>
<li>Skill equivalent to Network security and networking experience required specifically in a Wireless Service Provider environment. </li>
<li>Must possess the experience and ability to lead and participate in technical sessions, to lead projects that span multiple organizations, and to coordinate technical, implementation and/or troubleshooting discussions. </li>
<li>A strong understanding of structured network design and the OSI Interconnect Model, especially how it relates to the design and deployment of structured networks, is required as well as strong project management skills, strong interpersonal and communications skills (written and oral), and the ability to handle multiple projects simultaneously. </li>
 </ol>
                        </p>
                        <p> <span class="txt_clr"><strong>Job Responsibilities</strong></span></p>
                        <ol>
                            <li>Lead efforts to provide network security designs, solutions, deployment strategies, and technical documentation for the Client IP Security Networks in our various MSCs and Network Equipment Centers.</li>
                            <li>Provide technical leadership and guidance to other members of the Security team.	</li>
                            <li>Develop and maintain security documentation and standards documents, as required.</li> 
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Qualifications and Skills </strong> </span>
                        <ol>
                        <li>Certification: CCNA / CCNP / CCIE Preferred, firewall certification preferred.</li>
                        <li>Routing, Switching, Juniper Firewall</li> 
                        </ol>
                        </p>
                        </p>
                        <p><span class="txt_clr"><strong> Additional skills (Good to have)  </strong> </span>
                        <ol>
                        <li>Shall include cloud and scripting experience, Linux, Openstack, Python, Ansible and virtualization.</li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary" >Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>