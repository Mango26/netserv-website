<!DOCTYPE html>
<html lang="en">

<head>
       <!-- meta tag -->
       <meta charset="utf-8">
       <title>NetServ - Full Stack Developer</title>
       <meta name="description" content="We are looking for a highly skilled computer programmer who is comfortable with both front and back-end programming and develop web architecture.">
       <!-- responsive tag -->
       <meta http-equiv="x-ua-compatible" content="ie=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <!-- favicon -->
       <link rel="apple-touch-icon" href="">
       <link rel="canonical" href="https://www.ngnetserv.com/full-stack-developer">
       <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
       <?php include 'service_csslinks.php'; ?>
       <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
       .bg4 {
              background-image: url(assets/images/bg/bg4.png);
       }

       .rs-collaboration.style1 .img-part img {
              position: relative;
              bottom: 0px;
       }

       .rs-services.style22 .service-wrap .icon-part img {
              width: 53px;
              height: 53px;
              max-width: unset;
       }

       ul.listing-style li {
              position: relative;
              padding-left: 30px;
              line-height: 34px;
              font-weight: 500;
              font-size: 14px;
       }

       ul.listing-style.regular2 li {
              font-weight: 400;
              margin-bottom: 0px;
       }

       .rs-about.style10 .accordion .card .card-body {
              background: #ffffff;
       }
</style>

<body class="home-eight">
       <!-- Preloader area start here -->
      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
       <!--End preloader here -->
       <!--Full width header Start-->
       <div class="full-width-header header-style4">
              <!--header-->
              <?php include 'header.php'; ?>
              <!--Header End-->
       </div>
       <!--Full width header End-->
       <!-- Main content Start -->
       <div class="main-content">
              <!-- Services Section Start -->
              <div class="rs-pricing style1">
                     <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
                            <div class="container">
                                   <div class="sec-title">
                                          <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                                          <h1 class="title white-color mb-0 text-center"> Full Stack Developer </h1>
                                          <div class="sub-title text-center white-color"> United States (Remote) | 3+ YEARS EXPERIENCE</div>
                                   </div>
                            </div>
                     </div>
              </div>

              <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
                     <div class="container">
                            <div class="row">
                                   <div class="col-lg-12">

                                          <div class="text-left">
                                                  <p> <span class="txt_clr"><strong>Designation</strong> : </span> Full Stack Developer </p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  3+ years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> United States (Remote) </p>
                                             <p> <span class="txt_clr"><strong> Job Description</strong></span>
                                                 </p>
                                                 <p>
                                                 We are looking for a highly skilled computer programmer who is comfortable with both front and back-end programming. Full stack developers are responsible for developing and designing front end web architecture, ensuring the responsiveness of applications, and working alongside graphic designers for web design features, among other duties.
                                                 Full stack developers will be required to see out a project from conception to final product, requiring good organizational skills and attention to detail. 
                                                 </p>
                                                 <!-- <p>
                                                        As part of Cyber Security Center of Excellence team, your responsibilities includes providing technical guidance, technology evaluations, Proof of Concepts development, Cloud distributed application architecture, design, design review, coding practices, writing code, code review, continuous integration, continuous deployment, automated testing, scaling the products and solutions for SaaS products and solutions.
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong>Primary Skills :</strong> </span>
                                                 <ol>
                                                        <li>Deep understanding and experience of architecting and developing full stack end to end scalable and distributed Cloud application serviced out of Amazon AWS</li>
                                                        <li>Solid SaaS Application architecture and Cloud Deployment architecture principles with deep rooted experience on Amazon AWS</li>
                                                        <li>Expertise in loosely coupled design, Micro-services development, Message queues and containerized applications deployment using technologies like RESTful services, Message Queues, and Docker</li>
                                                        <li>Strong computer science fundamentals, and algorithms</li>
                                                        <li>Hands on deep expertise on Python and Python Web, Django</li>
                                                        <li>Experience working with SQL Databases like MySQL and PostgreSQL</li>
                                                        <li>Experience working with NoSQL Databases like MongoDB, Cassandra</li>
                                                        <li>Good understanding of HTML5, CSS3, JavaScript, OOJS.</li>
                                                        <li>Good familiarity with Linux operating system</li>
                                                        <li>Understanding and awareness of Secure software development lifecycle and web application vulnerabilities counter measures, e.g. OWASP Top 10 Security Risks</li>
                                                        <li>Understanding of CapEx and Opex estimates for applications.</li>
                                                        <li>Good understanding of licensing and subscription</li>
                                                        <li>Should be strong in financial planning for application hosting and server configurations.</li>
                                                        <li>Should Have strong knowledge of AWS Billing cycle, TCO. Cost Calculator, Pricing principles and AWS purchasing options (on-demand, reserve and spot bidding).</li>
                                                        <li>Experience with CI/CD pipeline with detailed understanding of Git, Jenkins, TeamCity, Artifactory, Cloudformation & Terraform.</li>
                                                        <li>Test Driven Development (TDD) mindset and orientation of 100% test automation</li>
                                                 </ol>
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong>Experience :</strong> </span>
                                                 <ol>
                                                        <li>Experience with Active Directory consolidation/migration to Azure AD</li>
                                                        <li>Experience with technical architecture experience integrating identity management, access management, and access governance software into clients&#39; infrastructure and applications.</li>
                                                        <li>Experience with documenting current and proposed on-premise and cloud environments including Visio Diagrams, Build documents, Recommendations for solutions, and run book.</li>
                                                        <li>Identity Management familiarity in one or more of the following areas:</li>
                                                        <li>Identity Governance and Administration (Lifecycle Management and Provisioning)</li>
                                                        <li>Single Sign-on and Advanced Authentication (Federation and Risk-Based Authentication)</li>
                                                        <li>Privileged Access Management</li>
                                                        <li>Enterprise Directory Architecture and Virtual Directories</li>
                                                        <li>Identity &amp; Access Governance including Role-based access control, access request, and certification.</li>
                                              
                                                   
                                                 </ol>
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong>Secondary Skills :</strong> </span>
                                                 <ol>
                                                        <li>Knowledge of JavaScript and frontend frameworks React and Angular</li>
                                                        <li>Knowledge of Java and Java Web is an advantage</li>
                                                 </ol>
                                                 </p> -->
                                                 <p><span class="txt_clr"><strong>Full Stack Responsibilities :</strong> </span>
                                                 <ol>
                                                        <li>Developing front end website architecture.
                                                  </li>
                                                        <li>Designing user interactions on web pages.</li>
                                                        <li>Developing back-end website applications.</li>
                                                        <li>Creating servers and databases for functionality.</li>
                                                        <li>Ensuring cross-platform optimization for mobile phones.</li>
                                                        <li>Ensuring responsiveness of applications.
                                                        </li>
                                                        <li>Working alongside graphic designers for web design features.
                                                        </li>
                                                        <li>Seeing through a project from conception to finished product.
                                                        </li>
                                                        <li>Designing and developing APIs.
                                                        </li>
                                                        <li>Meeting both technical and consumer needs.
                                                        </li>
                                                        <li>Staying abreast of developments in web applications and programming languages.
                                                        </li>
                                                 </ol>
                                                 </p>
                                                 <p><span class="txt_clr"><strong>Full Stack Developer Requirements:</strong> </span>
                                                 <ol>
                                                        <li>Degree in computer science.
                                                  </li>
                                                        <li>Strong organizational and project management skills.</li>
                                                        <li>Must have • 3 years + experience in a similar role </li>
                                                        <li>Deep understanding of Javascript</li>
                                                        <li>Client technology such as Angular, HTML5, Typescript </li>
                                                        <li>Database knowledge (SQL, CosmosDB, …) Good to have
                                                        </li>
                                                        <li>Experienced with Microsoft Teams or Skype for Business Server 
                                                        </li>
                                                        <li>Experienced with NodeJS, gRPC and RxJS or SignalR
                                                        </li>
                                                        
                                                 </ol>
                                                 </p>
                                                 <!-- <p><span class="txt_clr"><strong> Educational/professional qualification required :</strong> </span>
                                                 <ol>
                                                        <li>Bachelor in Computer Science or equivalent. Preferably a Master degree in computer science.</li>

                                                 </ol>
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong> Skills and Qualification:</strong> </span>
                                                 <ol>
                                                        <li>SIEM - Splunk /QRadar/Sentinel Certification.</li>
                                                        <li>Must have a technical working knowledge SIEM, EDR, antimalware, penetration testing,
                                                 vulnerability scans, ACLs, and IDS/IPS Concepts.
                                                 </li>
                                                 <li>CEH/ OSCP/ CISSP/CISM and other relevant Certifications.
                                                 </li>
                                                 </ol>
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong> Certification :</strong> </span>
                                                 <ol>
                                                        <li>Candidates with AWS Solution Architect (Associate/Professional) would be prefered.</li>

                                                 </ol>
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong>Soft skills/competencies :</strong> </span>
                                                 <ol>
                                                        <li>Problem solving mind and attitude</li>
                                                        <li>Effective communication skills – written, spoken, listening and presentation</li>
                                                        <li>Great Team player and experience working with global teams and global organizations</li>
                                                        <li>Genuine interest in learning and knowledge sharing
                                                               Must have written technical paper, blogs, speaker in technical forums, or patents</li>
                                                 </ol>
                                                 </p> -->
                                          </div>
                                          <div class="btn-part">

                                                 <a class="btn btn-primary" href="mailto:hr@ngnetserv.com">Apply</a>
                                          </div>
                                   </div>

                            </div>
                     </div>
              </div>

       </div>
       <!-- Main content End -->
       <!-- Footer Start -->
       <?php include 'footer.php'; ?>
       <!-- Footer End -->
       <!-- start scrollUp  -->
       <div id="scrollUp">
              <i class="fa fa-angle-up"></i>
       </div>
       <!-- End scrollUp  -->
       <?php include 'service_jslinks.php'; ?>
</body>

</html>