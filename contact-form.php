<?php

// session starting 
session_start();

// variables if needed 
$fname ='';
$email ='';
$message = '';
$errors = array();

// connect to database 
$db = mysqli_connect('localhost', 'root', '', 'contact-form');

// main form code 

if (isset($_POST['reg_user'])) {
      // getting inputs from form 
       $fname = mysqli_real_escape_string($db, $_POST['fname']);
       $email = mysqli_real_escape_string($db, $_POST['email']);
       $message = mysqli_real_escape_string($db, $_POST['message']);

       // form validation:
       if (empty($fname)) {
              array_push($errors, "Name is required");
       }
       if (empty($email)) {
              array_push($errors, "Email is required");
       }
       if (empty($message)) {
              array_push($errors, "Message is required");
       }

       // saving users info 
       $query = "INSERT INTO contact (fname, email, message) 
  			  VALUES('$fname', '$email', '$message')";

       mysqli_query($db, $query);
// success message 
       $_SESSION['success'] = "Will revert back shortly";
}