<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Security Architect  </title>
    <meta name="description" content="We are seeking a Cloud and Network Security Architect to define the Cloud Security Strategy, Risk and Compliance.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/network-architect"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Security Architect</h1>
                    <div class="sub-title text-center white-color"> United States (Remote) | 8+ years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <p>We are seeking a Cloud and Network Security Architect to define the Cloud Security Strategy, Risk and Compliance.</p>
                        <p> <span class="txt_clr"><strong>Designation</strong> : </span>  Cloud and Network Security Architect</p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry</p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  8+ years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span>United States (Remote)  </p>
                        <p> <span class="txt_clr"><strong>Job Responsibilities</strong></span>
                        </p>
                        <ol>
                            <li>Enhances security team accomplishments and competencies by planning the delivery of solutions and answering technical questions.</li>
                            <li>Plans, researches, and designs security architecture for IT systems. </li>
                            <li>Develops, reviews, and approves installation requirements for LANs, WANs, VPNs, firewalls, routers, and related network devices.</li>
                            <li>Determines security protocols by evaluating business strategies and requirements. </li>
                            <li>Responds to, and investigates, security incidents and provides thorough post-event analyses. </li>
                            <li>Develop project timelines for ongoing system upgrades. </li>
                            <li>Reviews system security measures and implements necessary enhancements. </li>
                            <li>Conducts regular tests and monitoring of network security. </li>
                            <li>Verifies security systems by developing and implementing test scripts. </li>
                            <li>Updates job knowledge by tracking and understanding emerging security practices and standards, participating in educational opportunities, reading professional publications, and participating in professional organizations.</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Qualifications and Skills </strong> </span>
                        <ol>
                            <li>Strong working knowledge of IT risks, cyber security, and computer operating software. </li>
                            <li>Advanced understanding of security protocols, cryptography, and security. </li>
                            <li>Experience implementing multi-factor authentication. </li>
                            <li>Great communication and interpersonal skills.</li>
                            <li>Comfortable working on a team.</li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary" >Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>