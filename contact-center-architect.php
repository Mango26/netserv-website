<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Contact Center Architect  </title>
    <meta name="description" content="Responsibilities will include architecting call center solutions coordinating with the IT Unified Communications Architecture, IT Operations, and the telephony and Contact Center product communities to design and drive the implementation of Contact Center solutions that meet the strategic needs of the business.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/network-architect"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
    ul.b {
        list-style-type: square;
        margin-left: 2rem;
    }
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Contact Center Architect</h1>
                    <div class="sub-title text-center white-color"> US (Remote) | 8+ years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <p>This position will support all projects related to the Contact Center telephony program. Candidates should have prior experience as in a Contact Center environment and have a deep technical understanding of Contact Center functionality. This program is primarily an internal IT program. Responsibilities will include architecting call center solutions coordinating with the IT Unified Communications Architecture, IT Operations, and the telephony and Contact Center product communities to design and drive the implementation of Contact Center solutions that meet the strategic needs of the business. </p>
                        <p> <span class="txt_clr"><strong>Designation</strong> : </span>  Contact Center Architect</p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in the Market </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  8+ years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> US (Remote)  </p>
                        <p> <span class="txt_clr"><strong>Skills </strong></span>
                        </p>
                        <ol>
                            <li>Cloud based Contact Center solutions – Design, Deployment, and Operations </li>
                            <li>Skills:  Microsoft Teams, Audio Codes, Anywhere 365. Avaya PBX, Cisco Voice</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>

Job Description </strong> </span>
                        <ol>
                            <li>Excellent English communication skills, both verbal and written while speaking.</li>
                            <li>Works independently, proactively researching solutions, pulling in assistance appropriately. . </li>
                            <li>Able to Lead conversations and meetings. </li>
                            <li>Strong knowledge and experience working with VoIP networking products and solutions including voice calling, voice routing, voice mail services and features .</li>
                            <li>Knowledge of dial plans and ability to interpret and configure a custom dial plan.</li>
                            <li>Communicate with a broad spectrum of stakeholders. </li>
                            <li>Ability to work with minimum supervision in a team environment, demonstrating innovation, initiative, a customer service focus, positive ambassadorship and the ability to meet commitments </li>
                            <li>Candidates will use their knowledge and experience to help drive product delivery and technology direction in MS Teams, Skype for Business, Audio codes, Contact Center. </li>
                            <li>Utilize experience of Contact Center systems such as Contact Routing and CTI, Automatic Call Distribution (ACD), Interactive Voice Response (IVR), Call Recording, Workforce Management and Quality Assurance for multichannel contact management of voice, email, chat, SMS messaging, etc. to provide guidance, thought leadership and operational support as a highly skilled Contact Center advisor for internal and external customers. </li>
                            <li>Be capable of driving large contact center projects from an architecture and design perspective including:
                                <ul class="b">
                                    <li>Call routing </li>
                                    <li>Multi-site virtual centers </li>
                                    <li>Inbound/outbound self-service/queuing and applications </li>
                                    <li>Agent desktop </li>
                                    <li>Deployment approach </li>
                                </ul>
                            </li>
                            <li>Candidates will be directly engaging with all stakeholders – security, compliance, data, user experience, business, engineering, governance. </li>
                            <li>Developing short and long-term strategic Microsoft technology roadmaps, backed by pragmatic designs that support our enterprise technology roadmaps and key business objectives
                                Deep level of understanding of voice technologies: TDM, SIP, QoS, SBC, Codecs, Voice Recording and general IPT </li>
                            <li>Participate and contribute to the running of a cross-region Architecture function that will drive and govern the Digital Workplace Services and provide deep technology leadership and vision
                                Building plans and policies for Microsoft Teams and associated voice and video communications services, strategies and technologies. </li>
                            <li>Establishing a consistent and progressive strategy and architecture for unified communications, exploiting new market opportunities as appropriate. </li>
                            <li>Strategically plan, develop and drive implementation of new initiatives to enhance the global business. This includes projects related to investments and improvements
                                Working knowledge Office-365 and Microsoft Collaboration suites and experience in managing Collaboration Technologies.</li>
                            <li>Exposure to solution architect, design (HLD/LLD), consultancy and migration across full life cycle projects (On-premises, Hybrid, and Online). </li>
                            <li>Tools for Monitoring & Administration of Teams, contact center, audio codes. </li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary" >Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>