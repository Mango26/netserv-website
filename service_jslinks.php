<!-- modernizr js -->
<script src="<?php echo main_url; ?>/assets/js/modernizr-2.8.3.min.js"></script>
<!-- jquery latest version -->
<script src="<?php echo main_url; ?>/assets/js/jquery.min.js"></script>
<!-- Bootstrap v4.4.1 js -->
<script src="<?php echo main_url; ?>/assets/js/bootstrap.min.js"></script>
<!-- Menu js -->
<script src="<?php echo main_url; ?>/assets/js/rsmenu-main.js"></script>
<!-- op nav js -->
<script src="<?php echo main_url; ?>/assets/js/jquery.nav.js"></script>
<!-- Js for form  -->
<script src="<?php echo main_url; ?>/assets/js/form-validator.min.js"></script>
<script>
    var scripts = [
        "<?php echo main_url; ?>/assets/js/slick.min.js",
        "<?php echo main_url; ?>/assets/js/imagesloaded.pkgd.min.js",
        "<?php echo main_url; ?>/assets/js/wow.min.js",
        "<?php echo main_url; ?>/assets/js/aos.js",
        "<?php echo main_url; ?>/assets/js/plugins.js",
        "<?php echo main_url; ?>/assets/js/typewriter.min.js",
        "<?php echo main_url; ?>/assets/js/waypoints.min.js",
        "<?php echo main_url; ?>/assets/js/plugins.js",
        "<?php echo main_url; ?>/assets/js/main.js",
        "<?php echo main_url; ?>/assets/js/typewriter.min.js",
        "https://www.google.com/recaptcha/api.js",
        "<?php echo main_url; ?>/assets/js/contact.form.js",
        "<?php echo main_url; ?>/assets/js/contact-form-script.js"
    ];
    scripts.forEach((script) => {
        let tag = document.createElement("script");
        tag.setAttribute("src", script);
        document.body.appendChild(tag);
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script>
    $(function() {
        var srcText = $(".tagline-text").html();
        if(typeof srcText != 'undefined'){        
        $(".tagline-text").empty();
        var i = 0;
        var result = srcText[i];
        setInterval(function() {
                if(i == srcText.length) {
                    clearInterval(this);
                    return;
                }
                i++;
                result += srcText[i].replace("\n", "<br />");
                $(".tagline-text").html( result);
            },
            50);
        }
    });
         var totop = $('#footer-id');
        totop.on('click', function() {
        $("html,body").animate({
            scrollTop: 0
        }, 500)
         });
</script>