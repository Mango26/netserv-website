<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Network Architect </title>
    <meta name="description" content="Analyze business requirements to develop technical network solutions and standard frameworks.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/network-architect"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Network Architect </h1>
                    <div class="sub-title text-center white-color">United States (Remote) | 8 - 10 Year experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                         <p><span class="txt_clr"><strong>Designation</strong> : </span> Network Architect</p>
                        <p><span class="txt_clr"><strong>Salary </strong> : </span> Best in Industry </p>
                        <p><span class="txt_clr"><strong>Experience </strong> : </span> 8 - 10 Years </p>
                        <p><span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p><span class="txt_clr"><strong>Location  </strong> : </span> United States (Remote) </p>
                        <p><span class="txt_clr"><strong>Skills </strong></span>
                        </p>
                        <p> <span class="txt_clr"><strong>Roles and Responsibilities</strong></span>
                        </p>
                     
                        <ol>
                            <li>Design, test, and inspect data communications systems. Ability to create design (HLD, LLD) for Target state network architecture.</li>
                            <li>Evaluating, preparing, presenting, and providing solution designs.</li>
                            <li>Provide deep expertise in network design conceptual, proof-of-concept, and network deployment mythologies.</li>
                            <li>Managing information and network security</li>
                            <li>Develop a strong working relationship with engineering, manufacturing, and cross IT functions.</li>
                            <li>Partner with cyber security to detect, prevent and defend security network vulnerability.</li>
                            <li>Troubleshoot complex network issues related to routing, switching, and wireless infrastructure.</li>
                            <li>Provide technical expertise and fully engage during P1/P2 network escalation.</li>
                            <li>Engage with external vendors on technology evaluation and recommendation.</li>
                            <li>Hands-on experience with network automation, CI/CD pipeline, NAC, Firewalls and SD-WAN.</li>
                            <li>Managed MPLS networks service, OSPF, BGP routing, Datacenter Switches</li>
                            <li>Design & deployment experience with Datacenter, Large Campus setup</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Qualification</strong> </span>
                        <ol>
                            <li>Minimum 8 - 10 years experience working with multi-vendors technologies including routing, switching and security.</li>
                            <li>Data Center, Campus, Manufacturing, Engineering Network Design and Deployment</li>
                            <li>Hands-on experience with setting up Cloud connectivity as AWS/Azure.</li>
                            <li>Certifications in CCNP, Azure/AWS Networking certifications beneficial.</li>
                            <li>Proven experience in LAN, WAN, WLAN, and WWAN design and implementation.</li>
                            <li>Proven experience with network capacity planning, network security principles, and general network management best practices.</li>
                            <li>Proven experience in Azure/AWS Cloud Solutions, Azure/AWS Network Services and Integrations</li>
                            <li>Experience in advanced switching, routing, VRF, IPSec VPN.</li>
                            <li>Hands on experience on Juniper/Cisco/palo Alto firewalls and their integration with other well-known security systems.</li>
                            <li>Ability to execute in a methodical manner to gather, document, and present specific requirements as well as articulate technical solutions.</li>
                            <li>Exceptional documentation skills that can clearly articulate technical designs, issues and constraints, procedures, and network assessments.</li>
                            <li>Strong analytical and problem determination/resolution skills.</li>
                            <li>Excellent presentation and communication skills regarding technical and non-technical concepts</li>
                            <li>Excellent interpersonal skills and the ability to establish professional working relationships with varying levels of both business and technical personnel.</li>
                            <li>Self-directed to complete tasks in both a professional and timely manner with minimal supervision.</li>
                            <li>Candidates must be willing to be on call (in rotation with other team members) including night and weekends to support customer environments after hours. Night and weekend work will occur.</li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary" >Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>