<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>NetServ - Senior Network Architect</title>
    <meta name="description"
          content="NetServ Inc is looking for a Network Architect who can configure, manage, and support network devices across the Campus and DC environments.">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/senior-networkarchitect"/>
    <link rel="shortcut icon" type="image/x-icon"
          href="../assets/images/favicon.png"> <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>{
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }</script>
</head>
<style type="text/css"> .bg4 {
        background-image: url(assets/images/bg/bg4.png)
    }

    .rs-collaboration.style1 .img-part img {
        position: relative;
        bottom: 0
    }

    .rs-services.style22 .service-wrap .icon-part img {
        width: 53px;
        height: 53px;
        max-width: unset
    }

    ul.listing-style li {
        position: relative;
        padding-left: 30px;
        line-height: 34px;
        font-weight: 500;
        font-size: 14px
    }

    ul.listing-style.regular2 li {
        font-weight: 400;
        margin-bottom: 0
    }

    .rs-about.style10 .accordion .card .card-body {
        background: #fff
    }</style>
<body class="home-eight">
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<div class="full-width-header header-style4"> <?php include 'header.php'; ?> </div>
<div class="main-content">
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title"><h1 class="title white-color mb-0 text-center" style="font-size: 36px;"> Senior
                        Network Architect </h1>
                    <div class="sub-title text-center white-color"> United States (Onsite/Hybrid) | 4+ Years experience
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left"><p><span class="txt_clr"><strong>Designation</strong> : </span> Senior Network Architect </p>
                        <p><span class="txt_clr"><strong>Salary </strong> : </span> Best in Industry </p>
                        <p><span class="txt_clr"><strong>Experience </strong> : </span> 4+ Years </p>
                        <p><span class="txt_clr"><strong>Joining </strong> : </span>Immediate/15 days </p>
                        <p><span class="txt_clr"><strong>Location </strong> : </span> United States (Onsite/Hybrid) </p>
                        <p><span class="txt_clr"><strong>Job Description</strong></span><br>Looking for a Network
                            Architect who can configure, manage, and support network devices across the Campus and DC
                            environments.</p> <br>
                        <ol class="listing-style2 pt-0" style="margin-top: -25px;">
                            <li>Perform performance tuning and preventive maintenance on network devices, establishing
                                performance thresholds, monitoring performance, and diagnosing and correcting network
                                slowdowns/congestion.
                            </li>
                            <li>Participate in the defining, development, and publication of procedures and standards
                                related to router and switch installation and support.
                            </li>
                            <li>Respond to customer calls/incident tickets from the services desk/ITSM system.</li>
                            <li>Coordinate with other teams and OEM manufacturers when onsite support activities are
                                required.
                            </li>
                            <li> Provide troubleshooting and incident resolution support for network issues in the
                                environment.
                            </li>
                            <li>Perform root cause analysis and ensure continuous updating of asset and configuration
                                management systems.
                            </li>
                            <li> Interact with security, systems administration, and applications development teams to
                                restore service and/or identify and correct complex and recurring network problems.
                            </li>
                            <li>Evaluate and recommend new hardware and software solutions with potential to enhance
                                network performance.
                            </li>
                            <li>For Network Engineers supporting data center networks, work with data center operations
                                teams to configure networking equipment and load balancing for new server deployments,
                                and participate in data center capacity planning based on network utilization and
                                performance thresholds.
                            </li>
                            <li>using tools like JIRA in following Agile Methodologies</li>
                            <li>For Network Engineers supporting data center networks, configure, manage, and support
                                VLANS, load balancers, and associated data center network devices, and provide DNS,
                                DHCP, and IP address management services.
                            </li>
                            <li>For Network Engineers supporting firewalls, configure and manage firewall rules, and
                                participate in the application development and testing cycle to understand the firewall
                                rule requirements and implement them correctly and fully.
                            </li
                            <li>For Network Engineers supporting SHC’s DAS system, manage frequency bands and DAS
                                protocols for a wide range of mobile and wireless technologies, and manage, optimize,
                                and report on DAS performance and interference.
                            </li>
                        </ol>
                        <p><span class="txt_clr"><strong>Education/Qualification :</strong> </span>
                        <ol class="listing-style2 ">
                            <li> Bachelor's degree in a work-related field/discipline from an accredited college or
                                university
                            </li>
                        </ol>
                        </p><p><span class="txt_clr"><strong>Experience</strong> </span>
                        <ol>
                            <li>Four plus years of progressively responsible and directly related work experience.</li>
                        </ol>
                        </p><p><span
                                    class="txt_clr"><strong>Required Knowledge, Skills, and Abilities :</strong> </span>
                        <ol>
                            <li> Ability to analyze and develop solutions to complex problems</li>
                            <li> Ability to analyze information, reach valid conclusions, and make a sound
                                recommendations
                            </li>
                            <li> Ability to apply judgment and informed decisions</li>
                            <li>Ability to establish and maintain effective working relationships</li>
                            <li>Ability to manage, organize, prioritize, multi-task, and adapt to changing priorities
                            </li>
                            <li>bility to work effectively as a team player</li>
                            <li>Strong experience managing and supporting the following toolsets, technologies, and
                                protocols, for example:
                            </li>
                            <ul>
                                <li>Cisco routers (e.g., 36XX, 76XX, ASR 1XXX)</li>
                                <li>Cisco switches (e.g., Catalyst, Nexus)</li>
                                <li>Associated networking devices (e.g., LAN Controllers, WAN Optimizers)</li>
                                <li>Firewalls (e.g., Cisco ASA, Catalyst)</li>
                                <li>Load Balancers (e.g., F5 Labs)</li>
                                <li>DAS devices and protocols (e.g., GSM, UMTS, LTE, CDMA, EVDO, WiMAX, LMR)</li>
                                <li>Data Center Network devices (e.g., VLANs, load balancers)</li>
                                <li> Cisco LMS, ACS, and WCS</li>
                                <li>Solarwinds</li>
                                <li>DHCP</li>
                            </ul>
                        </ol>
                        </p></div>
                    <div class="btn-part"><a href="mailto:hr@ngnetserv.com" class="btn btn-primary">Apply</a></div>
                </div>
            </div>
        </div>
    </div>
</div><?php include 'footer.php'; ?>
<div id="scrollUp"><i class="fa fa-angle-up"></i></div><?php include 'service_jslinks.php'; ?></body>
</html>