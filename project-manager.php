<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Project Manager</title>
    <meta name="description"
          content="Within NetServ, Project Managers are responsible for making amazing things take place with our customers.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/project-manager"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4 {
        background-image: url(assets/images/bg/bg4.png)
    }

    .rs-collaboration.style1 .img-part img {
        position: relative;
        bottom: 0
    }

    .rs-services.style22 .service-wrap .icon-part img {
        width: 53px;
        height: 53px;
        max-width: unset
    }

    ul.listing-style li {
        position: relative;
        padding-left: 30px;
        line-height: 34px;
        font-weight: 500;
        font-size: 14px
    }

    ul.listing-style.regular2 li {
        font-weight: 400;
        margin-bottom: 0
    }

    .rs-about.style10 .accordion .card .card-body {
        background: #fff
    }

    ul.b {
        list-style-type: square;
        margin-left: 2rem;
    }
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Project Manager</h1>
                    <div class="sub-title text-center white-color">United States (Remote) | 5+ years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <p>Within NetServ, Project Managers are responsible for making amazing things take place with
                            our customers. Our teams make world class products, and our project managers guide those new
                            technology into our customer’s production environments. </p>
                        <p><span class="txt_clr"><strong>Designation</strong> : </span> Project Manager</p>
                        <p><span class="txt_clr"><strong>Salary </strong> : </span> Best in Industry </p>
                        <p><span class="txt_clr"><strong>Experience </strong> : </span> 5+ years </p>
                        <p><span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p><span class="txt_clr"><strong>Location  </strong> : </span>United States (Remote) </p>
                        </p>
                        </p>
                        <p><span class="txt_clr"><strong>
   Job Description </strong> </span>
                        <ol>
                            <li>Manage of delivery project requirements.</li>
                            <li>Management of all stages through to project closure..</li>
                            <li>Manage business and project risks, and effective plan and risk assessment so that
                                timescales and project costs are understood and underwritten by all by the parties.
                            </li>
                            <li>Feed in to Change Control assessment, impacting against plan and costs.</li>
                            <li>Be accountable for tracking the project costs and ensures that the impact on the
                                Business Units P&L, due to project activities within the scope of the project, is
                                clearly visible and that all contingency usage is signed-off by the appropriate
                                governance authority e.g. Project Board.
                            </li>
                            <li>Identify, engage and successfully manage stakeholders. Build effective relationships and
                                leverage these to ensure the right outcome for the project and stakeholders.
                            </li>
                            <li>Develop detailed resource forecasts and estimates.</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>
                            Skills and Experience</strong> </span>
                        <ol>
                            <li>Experience of implementation of Infrastructure and Technical Service, especially Service
                                management and security aspects.
                            </li>
                            <li>Experience in managing large or complex projects through the full life-cycle.</li>
                            <li>Strong leadership and stakeholder management.</li>
                            <li>Experience leading and managing local and remote teams.</li>
                            <li>Have sound influencing and negotiation skills.</li>
                            <li>Experience in Handling Large Network and/or Data Centre Infrastructure projects.</li>
                            <li>Prince 2 or MSP Practitioner.</li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary">Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>