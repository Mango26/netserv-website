<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- meta tag -->
      <meta charset="utf-8">
      <title>NetServ - About Us</title>
      <meta name="description" content="We help businesses manage, leverage, and maximize their investment in Information Technology. We take great pride in being able to extend customized services that cover an ever-evolving technology landscape—empowering our customers to drive business outcomes.">
      <meta name="keywords" content="it services for business, it services and solutions, services manage, it operations, information technology, information system">
      <!-- responsive tag -->
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- favicon -->
      <link rel="apple-touch-icon" href="">
      <link rel="canonical" href="https://www.ngnetserv.com/about" />
      <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
      <?php include 'service_csslinks.php'; ?>
      <link rel="stylesheet" type="text/css" href="assets/css/about.css">
      <style type="text/css">
         .rs-breadcrumbs.bg-1 {
         background-image: linear-gradient(
         90deg, #e8e9eb 0%, #eaebed8c 50%, rgba(255, 255, 255, 0) 100%), url(assets/images/businessman-touching-virtual-screen.jpg);
         background-position: center;
         background-repeat: no-repeat;
         background-size: cover;
         }
         .rs-contact.style1 .contact-info .title {
                font-size: 28px;
            color: #ffffff;
            margin-bottom: 16px!important;
        }
      </style>
          <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
   </head>
   <body>  
   <!-- Google Tag Manager (noscript) -->
   <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
   <!-- End Google Tag Manager (noscript) -->

      <!-- Preloader area start here -->

      <!--End preloader here -->
      <!--Full width header Start-->
      <div class="full-width-header header-style4">
         <!--Header Start-->
         <?php include 'header.php';?>
         <!--Header End-->
      </div>
      <!--Full width header End-->
      <!-- Main content Start -->
      <div class="main-content about_cover">
      <div class="rs-project style1 bg13 pt-100 pb-100 md-pt-80 md-pb-80 ">
         <div class="container">
            <div class="row mb-60 md-mb-40 lg-col-padding">
            </div>
            <div class="row lg-col-padding">
               <div class="col-xl-6">
                  <div class="video-part">
                     <img src="assets/images/about.jpg" alt="About us">
                  </div>
               </div>
               <div class="col-xl-6 pl-55">
                  <div class="sec-title">
                     <h1 class="title mb-14">About us</h1>
                     <p class="desc mb-46">We empower businesses to get ahead and stay ahead of change by delivering mission-critical IT services. We are driven to lead organizations to modernize IT Operations, Data Center, Cloud and security environments. Our 90+ strong team of senior technology experts, cloud and data center architects, security architects, software developers, and technical trainers bring years of extensive experience to every customer engagement.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Counter Section Start -->
      <div class="rs-counter style1 modify  pt-92 pb-100 md-pt-72 md-pb-80 text-black">
         <div class="container">
            <div class="sec-title text-center mb-52 md-mb-29">
               <h2 class="title mb-0 ">What we do</h2>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <p class="pt-3">
                     Established in 2015, headquartered out of Silicon Valley, we help businesses manage, leverage, and maximize their investment in Information Technology. We take great pride in being able to extend customized services that cover an ever-evolving technology landscape—empowering our customers to drive business outcomes.
                  </p>
                  <div class="row what_we_do_laptop_view">
                     <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                        <div class="couter-part plus">
                           <div class="rs-count txt_blue">6</div>
                           <h5 class="title">Years of Experience</h5>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                        <div class="couter-part plus">
                           <div class="rs-count txt_blue">90</div>
                           <h5 class="title">Certified Engineers</h5>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 col-sm-6 xs-mb-30">
                        <div class="couter-part plus">
                           <div class="rs-count txt_blue">350</div>
                           <h5 class="title">Projects Completed
                           </h5>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 pl-55">
                  <img src="assets/images/about2.png" alt="What we do">
               </div>
            </div>
             <div class="what_we_do_tablet_view" style="display: none">
                 <div class="row ">
                     <div class="col-lg-4 col-md-4 col-sm-4 mb-30">
                         <div class="couter-part plus">
                             <div class="rs-count txt_blue">6</div>
                             <h5 class="title">Years of Experience</h5>
                         </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 mb-30">
                         <div class="couter-part plus">
                             <div class="rs-count txt_blue">90</div>
                             <h5 class="title">Certified Engineers</h5>
                         </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-4 xs-mb-30">
                         <div class="couter-part plus">
                             <div class="rs-count txt_blue">350</div>
                             <h5 class="title">Projects Completed
                             </h5>
                         </div>
                     </div>
                 </div>
             </div>

         </div>
      </div>
      <!-- Counter Section End -->
          <!--core-->
          <div id="rs-services" class="rs-counter style1 modify  pt-70 pb-10 md-pt-72 md-pb-80">
              <div class=" pb-100 md-pb-80">
                  <div class="container">
                      <div class="row ">
                          <div class="col-md-12 " align="center">
                              <h3 class="text-center">
                                  Our <span class="txt_clr"> Core Focus</span>
                              </h3>

                              <p>
                                  <span class="readon1 badge badge-pill badge-primary p-3 m-2">Managed SOC (SOC as a Service) </span>
                                  <span class="readon1 badge badge-pill badge-primary p-3 m-2">Managed AIOps
                                                        </span>
                                  <span class="readon1 badge badge-pill badge-primary p-4 p-md-3 p-lg-3 m-2">Startup Technical Support Services </span><br>
                                  <span class="readon1 badge badge-pill badge-primary p-4 p-md-3 p-lg-3 m-2"> Cloud, DC and Campus Consulting and Professional Services
                                                        </span>
                                  <span class="readon1 badge badge-pill badge-primary p-3 m-2">Training
                                                        </span>

                              </p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!--#core-->
      <!-- timeline-->
      <div class="rs-services style17 pt-100 pb-100 md-pt-70 md-pb-70">
         <div class="container">
            <div class="sec-title5 text-center mb-50">
               <h2 class="title title2 pb-20">NetServ History</h2>
               <div class="">Our customers rely on NetServ to provide bespoke, leading-edge business technology solutions to solve their business problems. With a huge focus on a customer-first approach, Our culture is based on steadfast employee commitment to treating every customer to achieve their business goals with utmost satisfaction and success.
               </div>
               <!-- test block-->
               <div class="row" id="time_line">
                  <div class="col-md-1 netserv_history_col_1">
                  </div>
                  <div class="col-md-10 netserv_history_col_10">
                     <!--timeline test-->
                     <div class="row no-gutters">
                        <div class="col-sm py-2">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2021</span></h4>
                                 <p class="card-text"><b>Hybrid Cloud Modernization</b><br>FSO, AppD, TE, IWO, Nexus Dashboard</p>
                              </div>
                           </a>
                        </div>
                        <!-- timeline item 1 center dot -->
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <!-- timeline item 1 event content -->
                        <div class="col-sm py-2">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 2 -->
                     <div class="row no-gutters">
                        <div class="col-sm py-2">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm margin_top">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2021</span></h4>
                                 <p class="card-text"><b>Full Stack Managed AIOps</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 2 -->
                     <div class="row no-gutters">
                        <div class="col-sm  padding_top">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2020</span></h4>
                                 <p class="card-text"><b>App Modernization</b><br>Cloud Security SASE</p>
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 2 -->
                     <div class="row no-gutters">
                        <div class="col-sm py-2">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm margin_top">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2020</span></h4>
                                 <p class="card-text"><b> Application Managed Services</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 3 -->
                     <div class="row no-gutters">
                        <div class="col-sm padding_top">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2019</span></h4>
                                 <p class="card-text"><b>APM, Automation Ansible and Terraform</b><br>Cloud Services Hybrid, Azure, GCP</p>
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm py-2">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 3 -->
                     <div class="row no-gutters">
                        <div class="col-sm">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm py-2 margin_top">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2019</span></h4>
                                 <p class="card-text"><b>Managed Security Services</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 4 -->
                     <div class="row no-gutters">
                        <div class="col-sm padding_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2018</span></h4>
                                 <p class="card-text"><b>Partner Enablement and Tranings</b><br>Viptela and Meraki SD-WAN</p>
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 4 -->
                     <div class="row no-gutters">
                        <div class="col-sm py-2">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm margin_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2018</span></h4>
                                 <p class="card-text"><b>Managed Cloud Services</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 3 -->
                     <div class="row no-gutters">
                        <div class="col-sm padding_top">
                           <a class="card card1">
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2017</span></h4>
                                 <p class="card-text"><b>DNAC and SD-Access</b><br>ISE and Stealth watch</p>
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm py-2">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 3 -->
                     <div class="row no-gutters">
                        <div class="col-sm">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm py-2 margin_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2017</span></h4>
                                 <p class="card-text"><b> Managed Infrastructure</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 4 -->
                     <div class="row no-gutters">
                        <div class="col-sm  padding_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <h4 class="card-title"><span class="txt_clr">2016</span></h4>
                                 <p class="card-text"><b>Network Information</b><br>Cloud and DC, ACI, VXLAN</p>
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 4 -->
                     <div class="row no-gutters">
                        <div class="col-sm py-2">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm margin_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2016</span></h4>
                                 <p class="card-text"><b> Managed Network Services</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 4 -->
                     <div class="row no-gutters">
                        <div class="col-sm padding_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <h4 class="card-title"><span class="txt_clr">2015</span></h4>
                                 <p class="card-text"><b>Network Transformation </b></p>
                              </div>
                           </a>
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm">
                        </div>
                     </div>
                     <!--/row-->
                     <!-- timeline item 4 -->
                     <div class="row no-gutters">
                        <div class="col-sm py-2">
                        </div>
                        <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                           <div class="row h-50">
                              <div class="col border-right">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                           <h5 class="m-2">
                              <span class="badge badge-pill bg-light border">&nbsp;</span>
                           </h5>
                           <div class="row h-50">
                              <div class="col">&nbsp;</div>
                              <div class="col">&nbsp;</div>
                           </div>
                        </div>
                        <div class="col-sm margin_top">
                           <a class="card card1" >
                              <div class="card-body">
                                 <div class=""></div>
                                 <h4 class="card-title"><span class="txt_clr">2015</span></h4>
                                 <p class="card-text"><b> Network Operations</b></p>
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--/row-->
                  </div>
                  <div class="col-md-1 netserv_history_col_1"></div>
                  <!-- test block-->
               </div>
               <!--#timeline test-->
               <div  align="center" class="pt-3">
                  <h5 class="txt_clr">Founded Netserv</h5>
               </div>
            </div>
         </div>
         <!--#timeline-->
         <div class="rs-questions style1 gray-bg pt-100 pb-100 md-pt-173 md-pb-80">
            <div class="container">
               <div class="row md-col-padding">
                  <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                     <div class="row gutter-20 hidden-xs">
                        <img src="../assets/images/business-people-meeting.png" alt="about us">
                     </div>
                  </div>
                  <div class="col-lg-6 pl-40">
                     <h2 class="title mb-21 md-mb-10">Our <span class="txt_clr">Core Expertise</span> include
                     </h2>
                     <h5 class="mb-2">Advanced Managed Services</h5>
                     <ul class="middle-part">
                        <li><i class="fa fa-check pr-3"></i>IT management and Service Desk support services
                        </li>
                        <li><i class="fa fa-check pr-3"></i>Managed AIOps
                        </li>
                        <li><i class="fa fa-check pr-3"></i>Application Managed Services
                        </li>
                        <li><i class="fa fa-check pr-3"></i>Infrastructure Managed Services
                        </li>
                     </ul>
                     <h5 class="mb-2 mt-3">Consulting and Professional Services</h5>
                     <ul class="middle-part">
                        <li><i class="fa fa-check pr-3"></i>Data Center
                        </li>
                        <li><i class="fa fa-check pr-3"></i>Cloud
                        </li>
                        <li><i class="fa fa-check pr-3"></i>Security
                        </li>
                        <li><i class="fa fa-check pr-3"></i>Campus
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="rs-whychooseus style4 pt-100 md-pt-72 md-pt-80 pb-100 lg-pb-93 md-pb-70">
            <div class="container">
               <div class="row md-col-padding">
                  <div class="col-lg-7 pr-110 lg-pr-30">
                     <div class="sec-title mb-40">
                        <h2 class="title mb-18">Why NetServ ?</h2>
                        <div class="desc">We have a combination of a core team that understands technology and business very well and a development program that builds strong technologists. We believe in outcome-based engagements fusing together the laser-focused passion for technology and multi-environment experience to enhance our clients’ business models.</div>
                        <div class="desc pt-2">We believe that our growth is deeply intertwined with your growth and we strive to provide the best of services to enable you to focus on what’s important - Driving your Business Objective & Strategic Initiatives</div>
                     </div>
                  </div>
                  <div class="col-lg-5 pl-0 md-mb-30 md-order-first">
                     <div class="image-part">
                        <img src="../assets/images/right-img.jpg" alt="Why NetServ ">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="rs-contact gray-bg  style1 pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
               <div class="white-bg">
                  <div class="row">
                     <div class="col-lg-8 form-part">
                        <div class="sec-title mb-35 md-mb-30">
                           <div class="sub-title primary">CONTACT US</div>
                           <h2 class="title mb-0">Get In Touch</h2>
                        </div>
                        <div id="form-messages"></div>
                        <?php include 'contact.php'; ?>
                     </div>
                     <div class="col-lg-4 pl-0 md-pl-pr-15 md-order-first">
                        <div class="contact-info ">
                           <div class="contact_txt_center">
                              <h3 class="title text-center " style="line-height: 44px;">
                                 If you have any questions about any of our services, Please complete the request form and one of our Technology experts will contact you shortly !
                              </h3>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Main content End -->
      <!-- Footer Start -->
      <?php include 'footer.php';?>
      <!-- Footer End -->
      <!-- start scrollUp  -->
      <div id="scrollUp">
         <i class="fa fa-angle-up"></i>
      </div>
      <!-- End scrollUp  -->
      <?php include 'home_jslinks.php'; ?>
<!--          <script src="https://www.google.com/recaptcha/api.js" async defer></script>-->
          <script>
              var scripts = [
                  "<?php echo main_url;?>/assets/css/animate.css",
                  "<?php echo main_url;?>/assets/css/aos.css",
                  "<?php echo main_url;?>/assets/css/slick.css",
                  "<?php echo main_url;?>/assets/css/off-canvas.css",
                  "<?php echo main_url;?>/assets/fonts/linea-fonts.css",
                  "<?php echo main_url;?>/assets/fonts/flaticon.css",
                  "<?php echo main_url;?>/assets/css/rsmenu-main.css",
                  "<?php echo main_url;?>/assets/css/rs-animations.css",
                  "<?php echo main_url;?>/assets/css/rsmenu-transitions.css",
                  "<?php echo main_url;?>/assets/css/rs-spacing.css",
              ];
              scripts.forEach((script) => {
                  let tag = document.createElement("link");
                  tag.rel  = 'stylesheet';
                  tag.type = 'text/css';
                  tag.media = 'all';
                  tag.setAttribute("href", script);
                  document.head.appendChild(tag);
              });
              var scripts = [
                  "<?php echo main_url; ?>/assets/js/slick.min.js",
                  "<?php echo main_url; ?>/assets/js/imagesloaded.pkgd.min.js",
                  "<?php echo main_url; ?>/assets/js/wow.min.js",
                  "<?php echo main_url; ?>/assets/js/aos.js",
                  "<?php echo main_url; ?>/assets/js/skill.bars.jquery.js",
                  "<?php echo main_url; ?>/assets/js/jquery.counterup.min.js",
                  "<?php echo main_url; ?>/assets/js/waypoints.min.js",
                  "<?php echo main_url; ?>/assets/js/plugins.js",
                  "<?php echo main_url; ?>/assets/js/main.js",
                  "https://www.google.com/recaptcha/api.js",
                  "<?php echo main_url; ?>/assets/js/contact-form-script.js",
              ];
              scripts.forEach((script) => {
                  let tag = document.createElement("script");
                  tag.setAttribute("src", script);
                  document.body.appendChild(tag);
              });
          </script>

   </body>
</html>