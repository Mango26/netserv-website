
<footer id="rs-footer" class="rs-footer">
    <div class="container">
        <div class="footer-content pt-80 pb-79 md-pb-64">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Company</h4>
                        <p class="mb-1"><a href="<?php echo main_url;?>/about" class="text-white">About</a></p>
                        
                        <p class="mb-1"><a href="<?php echo main_url;?>/terms" class="text-white">Terms</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/privacy-policy" class="text-white">Privacy Policy</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/contact-us" class="text-white">Contact</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Services</h4>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/consulting-services/consulting" class="text-white">Consulting</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/professional-services/professional-services" class="text-white">Professional Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/managed-services" class="text-white">Managed Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/software-development/software-development" class="text-white">Software Development</a></p>                        
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/training/training" class="text-white">Training</a></p>                        
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/services-startups/services-for-startups" class="text-white">Services for Startups</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Managed Services</h4>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/managed-infrastructure" class="text-white">Managed Infrastructure</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/managed-daas-and-vdi" class="text-white">Managed VDI/DaaS Services</a></p>

                        <!-- <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/application-management-services" class="text-white">
                                Application Managed Services</a></p> -->
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/managed-cloud-services" class="text-white">Managed Cloud Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/managed-security-services" class="text-white">
                                Managed Security Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/managed-services/full-stack-managed-services" class="text-white">Full Stack Managed AIOps</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Services for Startups</h4>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/services-startups/support-services-startups" class="text-white">Support Services</a></p>
                        <!-- <p class="mb-1"><a href="/services/services-startups/professional-services-startups" class="text-white">
                                Professional Services</a></p> -->
                         <p class="mb-1"><a href="<?php echo main_url;?>/services/software-development/software-development" class="text-white">Software Development</a></p>     
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/services-startups/partner-enablement" class="text-white">Partner Enablement</a></p>
                        <p class="mb-1"><a href="<?php echo main_url;?>/services/services-startups/managed-services-startups" class="text-white">
                                Managed Services</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row y-middle">
                <div class="col-lg-6 col-md-8 sm-mb-21">
                    <div class="copyright">
                        <p class="text-white" id="footer-id">© 2021 NetServ. All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4 text-right sm-text-center">
                    <ul class="footer-social">
                        <li><a href="https://twitter.com/netservllc"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/netserv-llc/"><i class="fa fa-linkedin"></i></a></li>
                        <li> <a href="https://www.youtube.com/channel/UCcTzZVwmHwFHK-YOYb51yOA/featured"><i class="fa fa-youtube-play"></i></a> </li>
                         <li> <a><i class="fa fa-facebook"></i></a> </li> 
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>