(function ($) {
  "use strict";
  $("#redemtion_submit")
    .validator()
    .on("submit", function (event) {
      if (event.isDefaultPrevented()) {
        formError();
        submitMSG(false, "Did you fill up the form properly?");
      } else {
        event.preventDefault();
        submitForm();
      }
    });
  function submitForm() {
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var email = $("#email").val();
    var job_title = $("#job_title").val();
    var message = $("#message").val();
    var phone_number = $("#phone_number").val();
    var country = $("#country").val();
    var comments = $("#comments").val();
    var agree_terms= $("#agree_terms").val();
    var d = grecaptcha.getResponse();

    $.ajax({
      type: "POST",
      url: "../../assets/php/redemtion_submit.php",
      data:
        "first_name=" +
          first_name +
          "&last_name=" +
          last_name +
        "&email=" +
        email +
          "&job_title=" +
          job_title +
        "&phone_number=" +
        phone_number +
          "&country=" +
          country +
        "&comments=" +
          comments +
        "&d=" +
        d +
        "&agree_terms=" +
          agree_terms,
      success: function (text) {
        if (text == "success") {
          formSuccess();
        } else {
          formError();
          submitMSG(false, text);
        }
      },
    });
  }
  function formSuccess() {
    $("#redemtion_submit")[0].reset();
    submitMSG(true, "One of our sales consultants will reach out to you to book a meeting at your convenience.");
  }
  function formError() {
    $("#redemtion_submit")
      .removeClass()
      .addClass("shake animated")
      .one(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
          $(this).removeClass();
        }
      );
  }
  function submitMSG(valid, msg) {
    if (valid) {
      var msgClasses = "h4 tada animated text-success";
    } else {
      var msgClasses = "h4 text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
  }
})(jQuery);
