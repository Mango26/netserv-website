<?php

$errorMSG = "";

// first_name
if (isset($_POST["first_name"])&& empty($_POST["first_name"])) {
    $errorMSG = "First name is required ";
} else {
    $first_name = $_POST["first_name"];
}

// first_name
if (isset($_POST["last_name"])&& empty($_POST["last_name"])) {
    $errorMSG = "Last name is required ";
} else {
    $last_name = $_POST["last_name"];
}

// email
if (isset($_POST["email"])&& empty($_POST["email"])) {
    $errorMSG = "Email address is required ";
} else {
    $email = $_POST["email"];
}

// job_title
if (isset($_POST["job_title"])&& empty($_POST["job_title"])) {
    $errorMSG = "Job Title is required";
} else {
    $job_title = $_POST["job_title"];
}

// phone_number
if (isset($_POST["phone_number"])&& empty($_POST["phone_number"])) {
    $errorMSG = "Job Title is required";
} else {
    $phone_number = $_POST["phone_number"];
}

// country
if (isset($_POST["country"])&& empty($_POST["country"])) {
    $errorMSG = "Country is required";
} else {
    $country = $_POST["country"];
}

// agree_terms
if (isset($_POST["agree_terms"])&& empty($_POST["agree_terms"])) {
    $errorMSG = "Agreement terms is required";
} else {
    $agree_terms = $_POST["agree_terms"];
}

// comments
if (empty($_POST["comments"])) {
    $comments = "Comment Not Available for this message";
} else {
    $comments = $_POST["comments"];
}

$EmailTo = "info@netserv.io";
//$EmailTo = "pooja.aplswd@gmail.com";
$Subject = "$email submitted redemption link";
$headers = "From:".$email . "\r\n";
// prepare email body text
$Body = "";
$Body .= "Full Name: ";
$Body .= $first_name." ".$last_name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Phone Number: ";
$Body .= $phone_number;
$Body .= "\n";
$Body .= "Country: ";
$Body .= $country;
$Body .= "\n";
$Body .= "Job Title: ";
$Body .= $job_title;
$Body .= "\n";
$Body .= "Comment: ";
$Body .= $comments;
$Body .= "\n";

// checking google recaptcha api
if (isset($_POST['d']) && !empty($_POST['d'])) {
    $secret = '6LfF3UcdAAAAAF2nYkyRSLO7AdzMyif9IhBzf6j1';
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['d']);
    $responseData = json_decode($verifyResponse);
    // checking response
    if ($responseData->success) {
        // if response is true send email
        $success = mail($EmailTo, $Subject, $Body, $headers);
        if ($success) {
            // echo json_encode(array("statusCode" => 200, 'msg' => "mail sent"));
            // redirect to success page
            // if ($success && $errorMSG == "") {
            //     echo "success";
            // } else {
            //     if ($errorMSG == "") {
            //         echo "Something went wrong :(";
            //     } else {
            //         echo $errorMSG;
            //     }
            // }
            //echo "One of our sales consultants will reach out to you to book a meeting at your convenience.";
            echo "success";
        } else {
            echo "Something went wrong :(";
        }
    } else {
        // echo json_encode(array("statusCode" => 402, 'msg' => "Robot verification failed please retry"));
        echo "Robot verification failed please retry";

    }
} else {
    // echo json_encode(array("statusCode" => 403, 'msg' => "please verify captcha"));
    echo "Please verify captcha";
}