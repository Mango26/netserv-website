<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Cloud Architect </title>
    <meta name="description" content="As a Cloud Solution Architect you will be responsible for the implementation of our IT Infrastructure Cloud Solutions and Services globally.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/cloud-architect"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;"> Cloud Architect </h1>
                    <div class="sub-title text-center white-color"> United States (Remote) | 10 Years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                         <p> <span class="txt_clr"><strong>Designation</strong> : </span>  Cloud Architect </p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span> 10 Years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> United States (Remote) </p>
                           
                        <p> <span class="txt_clr"><strong>Roles and Responsibilities</strong></span>
                        </p>
                     
                        <ol>
                            <li>As a Cloud Solution Architect you will be responsible for the implementation of our IT Infrastructure Cloud Solutions and Services globally.</li>
                            <li>You will be responsible for Data availability/enablement for business reporting within the SLA</li>
                            <li>Manage application and data integration platforms using AWS/Azure Cloud Components.</li>
                            <li>You will be a key member of our globally distributed team of Architects, Project Managers and Service Managers.</li>
                            <li>This candidate will actively work across the Cloud organization on identifying, designing, and implementing automated solutions.</li>
                            <li>You should be able to suggest some cost optimization areas and implementation plans</li>
                            <li>You would be responsible for suggesting & implementing automation.</li>
                            <li>You would be responsible to suggest some architecture changes to improve the existing architectures.</li>
                            <li>In your role as Cloud Solution Architect, you will report to the individual Steering Committee. </li>
                            
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Qualification and Experience :</strong> </span>
                        <ol>
                            <li>More than 10 years of experience in the IT infrastructure data center and network domain, creating design, architecture, and implementation.</li>
                            <li>4-5 years of experience in data modelling, data warehousing, and big data architectures.</li>
                            <li>4-5 years of experience with Amazon cloud (AWS)– S3, EC2, RDS, Lambda, DMS, Glue, Cloud Watch, SNS etc.</li>
                            <li>Understanding of RESTful API design/build.</li>
                            <li>Experience using GitHub, Bit Bucket, Terraform or other code repository solution.</li>
                            <li>Solid experience in IT infrastructure services in large global enterprises.</li>
                            <li>Bachelor’s degree in computer science or IT.</li>
                            <li>Presentation skills with a high degree of comfort presenting to both large and small audiences</li>
                            <li>Highly self-motivated.</li>
                            <li>Experience in IT Project and Service Management.</li>
                            <li>Proficient in application/software architecture (Definition, Business Process Modelling, etc.).</li>
                            <li>Experienced in working in DevOps environments.</li>
                            <li>Flexibility to work with people in different time zones, e.g. Europe.</li>
                            <li>Ability to drive change.</li>
                            <li>Strategic thinking.</li>
                            <li>Customer-oriented.</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Good to Have skills :</strong> </span>
                        <ol>
                            <li>Knowledge on python programming.</li>
                            <li>Knowledge on AWS lambda.</li>
                            <li>Knowledge of AWS Landing zone.</li>
                        </ol>
                        </p>
                        
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary" >Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>