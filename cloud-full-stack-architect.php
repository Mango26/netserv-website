<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Cloud Full Stack Architect</title>
    <meta name="description" content="In depth knowledge of NodeJS, ExpressJS, Angular and React.
        Hands on Experience with AWS Marketplace, Azure Cloud.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/cloud-full-stack-architect"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;"> Cloud Full Stack Architect </h1>
                    <div class="sub-title text-center white-color"> United States (Remote) | 10 - 15 Years  experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                         <p> <span class="txt_clr"><strong>Designation</strong> : </span> Cloud Full Stack Architect </p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span> 10 - 15 Years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span>  United States (Remote)  </p>
                           
                        <p> <span class="txt_clr"><strong>Job Description</strong></span>
                            <br>
                            Previous working experience on MERN stacks. <br>
                            In depth knowledge of NodeJS, ExpressJS, Angular and React. <br>
                            Hands on Experience with AWS Marketplace, Azure Cloud. <br>
                            Hands on Experience -
                        </p>
                        <ol class="listing-style2 pt-0" style="margin-top: -25px;">
                            <li>in implementing applications using Angular 6 or above</li>
                            <li>in implementing applications using React, React Native,Redux,MUI</li>
                            <li>in creating front end applications using HTML5, Angular, LESS / SASS, CSS</li>
                            <li>with JavaScript Development on both client and server-side</li>
                            <li> with modern frameworks and design patterns</li>
                            <li>with MongoDB, MySQL</li>
                            <li> in creating secure RESTful-based web services in XML and JSON, JavaScript, jQuery</li>
                            <li>in providing scalable solutions using Devops
                                using Continuous integration tools (Jenkins / Code Pipeline / Cicle CI / Ansible)
                            </li>
                            <li>using various Version Control Systems (SVN, Git, GitHub, Gitlab, Bitbucket. Bamboo etc.)</li>
                            <li>using tools like JIRA in following Agile Methodologies</li>
                        </ol>
                        <p><span class="txt_clr"><strong>Good knowledge :</strong> </span>
                        <ol class="listing-style2 ">
                            <li> of developing secure webpages</li>
                            <li>in integrating the security features into the web application
                                in SQL and NoSQL
                            </li>
                            <li>Consumer Web Development Experience for High-Traffic, Public Facing web applications</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>Nice to have :</strong> </span>
                        <ol>
                            <li>Knowledge of DynamoDB and MSSql.</li>
                            <li>Hands on experience in</li>
                            <li>AngularJS, Angular 2, Angular material design.</li>
                            <li>AWS IOT, Azure IOT or any Cloud IOT Platform</li>
                            <li>MQTT, HTTP, REST APIs</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>Responsibilities :</strong> </span>
                        <ol>
                            <li> Building interactive consumer data from multiple systems and RESTfully abstract to the UI.</li>
                            <li> Define code architecture decisions to support a high-performance and scalable product with a minimal footprint</li>
                            <li> Address and improve any technical issues</li>
                            <li>Collaborate well with Engineers, Architects, Business Units, Product Owners to design and create advanced, elegant, and efficient systems</li>
                            <li>Maximize Automation of entire Software lifecycle.</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>CI/CD DevOps Architect :</strong> </span>
                        <ol>
                            <li>Design, architect and implement next generation CICD Platform and automation solution</li>
                            <li>10+ Years of proven development and/or DevOps experience deploying and maintaining multi-tiered infrastructure and web applications</li>
                            <li>Strong hands-on experience in Linux/Unix/AWS environment and scripting languages: Shell,Python</li>
                            <li>Exposure in designing and implementing collaborated solutions on AWS and DevOps</li>
                            <li>Strong experience in automating Continuous Integration, Continuous Delivery and Agile practices for a highly scalable system</li>
                            <li>Perform analysis of the current practices and design and implement best practices and emerging concepts in CICD landscape</li>
                            <li>Experience with Docker, Microservices and container deployment and service orchestration</li>
                            <li>Experience with Blue-Green Deployment to reduce downtime and risk, provide continuous deployment and fast rollback</li>
                            <li>Experience with configuration management tools such as Puppet or Chef</li>
                            <li>Experience with Infrastructure automation (Terraform, Cloud formation)</li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary">Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>