# Netserv Website 

Website creation for netserv.io

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- https://gitlab.com/-/experiment/new_project_readme_content:577f506f2efea1efae2af22f2e2dda8a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file or https://gitlab.com/-/experiment/new_project_readme_content:577f506f2efea1efae2af22f2e2dda8a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file files
- https://gitlab.com/-/experiment/new_project_readme_content:577f506f2efea1efae2af22f2e2dda8a?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line or push an existing Git repository with the following command:


cd existing_repo
git remote add origin https://gitlab.com/aplswebdevp/netserv-website.git
git branch -M main
git push -uf origin main


## Integrate with your tools

Visual Studio Code is recommended

## Files not to push or changes

.htaccess
constants.php

## actively maintained branches

Only 2 branches one is main and other is minify which is minified version of main are bein actively managed

# the process

Please make the changes in main branch
Once the hcnages are done, minify the same file and paste it in the same file name and path of the branch mnify
Push both the changes
Branch minify is being pulled on our live server

# folder structure

The name of file can be derived from the URL
URL.php is the name of file in the directory
blog and cybersecurity news are not part of html and are maintained in wordpress
