<!DOCTYPE html>
<html lang="zxx">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Simplify And Secure IT Infrastructure.</title>
    <meta name="description" content="">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include '../../service_csslinks.php'; ?>
</head>
<style type="text/css">
    .rs-breadcrumbs.bg-3 {
        background-image: linear-gradient(90deg, #ffffff 0%, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0%) 100%), url(../../assets/images/young-people-working-call-center-new-deals-is-coming.jpg);
        background-size: cover;
        background-position: 10%;
    }
</style>

<body class="home-eight">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- Preloader area start here -->

    <!--End preloader here -->
    <!--Full width header Start-->
    <div class="full-width-header header-style4">
        <!--header-->
        <?php include '../../header.php'; ?>
        <!--Header End-->
    </div>
    <!--Full width header End-->

    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Section Start -->
        <div class="rs-breadcrumbs bg-3">
            <div class="container">
                <div class="content-part text-center">
                    <p><b>services - Consulting</b></p>
                    <h1 class="breadcrumbs-title  mb-0">Advisory Services
                    </h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs Section End -->

        <!-- Services Section Start -->
        <div id="rs-services" class="rs-services style1 modify2 pt-100 pb-84 md-pt-80 md-pb-64">
            <div class="container">
                <div class="row gutter-16">
                    <div class="sec-title style2 mb-60 md-mb-50 sm-mb-42">
                        <div class="first-half text-right">
                            <h2 class="title mb-0"><span class="txt_clr">Advisory</span> Services</h2>
                        </div>
                        <div class="last-half">
                            <p class="desc mb-0 pr-50">With expertise across all IT Disciplines, our consultants provide guidance to our clients to keep up with the fast-paced evolving IT technology landscape. To maximize business value, NetServ offers strategic advisory services turning your vision into an executable dynamic strategy that will transform your company into an intelligent, connected, secured enterprise.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="rs-services" class="rs-services style1 modify  pb-84 md-pt-72 md-pb-64">
            <div class="container">
                <div class="sec-title text-center mb-47 md-mb-42">
                    <h2 class="title mb-0">Cloud <span class="txt_clr">Modernization Strategy</span></h2>
                    <p class="p-3">Through our Cloud Advisory Services, we serve as a trusted advisor understanding your business goals, defining strategy, and providing advice for a wide range of organizational, process, and technical direction towards successful cloud modernization .

                    </p>
                </div>
                <div class="row gutter-16 justify-content-center">
                    <div class="col-lg-3 col-sm-6 mb-16">
                        <div class="service-wrap">
                            <div class="icon-part">
                                <img src="../assets/images/services/icons/modify/7.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="#">Cloud</a></h5>
                                <h6>Adoption strategy
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 mb-16">
                        <div class="service-wrap">
                            <div class="icon-part">
                                <img src="../assets/images/services/icons/modify/1.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="#">Data</a></h5>
                                <h6>Center modernization
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 mb-16">
                        <div class="service-wrap">
                            <div class="icon-part">
                                <img src="../assets/images/services/icons/modify/2.png" alt="">
                            </div>
                            <div class="content-part">
                                <h5 class="title"><a href="#">App</a></h5>
                                <h6>Modernization</h6>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Services Section End -->
        <!--1-->
        <div id="rs-about" class="rs-about style3 pt-100 lg-pt-90 md-pt-80 pb-92 md-pb-72 sm-pb-80">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 md-mb-30">
                        <div class="image-part">
                            <img src="../assets/images/about/h8-left-img.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15">
                        <div class="sec-title">
                            <!-- <div class="sub-title gray-color">About Us</div> -->
                            <h2 class="title mb-30">SD-Campus<span class="d-block blue-color">Transformation</span></h2>
                            <p class="desc2">Organizations are now constantly challenged by a lack of network agility and security. Our SD-Campus advisory service includes understanding & translating customer business objectives into technical requirements, migration & operational strategy to make SD-Campus transformation successful.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#1-->
        <!--1-->
        <div id="rs-about" class="rs-about style3 pt-100 lg-pt-90 md-pt-80 pb-92 md-pb-72 sm-pb-80">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15">
                        <div class="sec-title">
                            <!-- <div class="sub-title gray-color">About Us</div> -->
                            <h2 class="title mb-30">SD-Campus<span class="d-block blue-color">Transformation</span></h2>
                            <p class="desc2">Organizations are now constantly challenged by a lack of network agility and security. Our SD-Campus advisory service includes understanding & translating customer business objectives into technical requirements, migration & operational strategy to make SD-Campus transformation successful.</p>

                        </div>
                    </div>
                    <div class="col-lg-6 md-mb-30">
                        <div class="image-part">
                            <img src="../assets/images/about/h8-left-img.jpg" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--#1-->
        <!--1-->
        <div id="rs-about" class="rs-about style3 pt-100 lg-pt-90 md-pt-80 pb-92 md-pb-72 sm-pb-80">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 md-mb-30">
                        <div class="image-part">
                            <img src="../assets/images/about/h8-left-img.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15">
                        <div class="sec-title">
                            <!-- <div class="sub-title gray-color">About Us</div> -->
                            <h2 class="title mb-30">SD-Campus<span class="d-block blue-color">Transformation</span></h2>
                            <p class="desc2">Organizations are now constantly challenged by a lack of network agility and security. Our SD-Campus advisory service includes understanding & translating customer business objectives into technical requirements, migration & operational strategy to make SD-Campus transformation successful.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#1-->
    </div>

    <div id="rs-about" class="rs-about style1 bg1 md-pt-80">
        <div class="container">
            <div align="center">
                <h2 class="title mb-0 pt-75">Our broad range of
                    <span class="txt_clr">Advisory Services</span> include
                </h2>
            </div>
            <div class="row y-bottom pt-75 pb-79">
                <div class="col-lg-4 padding-0" align="center">
                    <div class="services-icon">
                        <img src="../assets/images/about/icons/1.png" alt="image">
                    </div>
                    <h4>Cloud migration</h4>
                </div>
                <div class="col-lg-4 padding-0" align="center">
                    <div class="services-icon">
                        <img src="../assets/images/about/icons/1.png" alt="image">
                    </div>
                    <h4>Cloud migration</h4>
                </div>
                <div class="col-lg-4 padding-0" align="center">
                    <div class="services-icon">
                        <img src="../assets/images/about/icons/1.png" alt="image">
                    </div>
                    <h4>Cloud migration</h4>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Main content End -->

    <!-- Footer Start -->
    <?php include '../../footer.php'; ?>
    <!-- Footer End -->

    <!-- start scrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End scrollUp  -->
    <?php include '../../service_jslinks.php'; ?>
</body>

</html>