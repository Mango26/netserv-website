<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Application Customization and Support</title>
    <meta name="description" content="Application customization and support services help customers to ensure that applications are reliable, highly available, scalable and relevant to evolving business needs.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/services/software-development/software-development#application-customization-support" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include '../../service_csslinks.php'; ?>
    <link rel="stylesheet" href="<?php echo main_url; ?>assets/css/assessment_services.css">
    <script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "WebSite",
  "name": "NetSev",
  "url": "http://www.ngnetserv.com/"
}
 </script>
</head>
<!-- Internal-css-starts -->
<style type="text/css">
    .rs-breadcrumbs.bg-3 {
        background-image: linear-gradient(90deg, #ffffff 0%, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0%) 100%), url(<?php echo main_url; ?>/assets/images/services/software-development/application_cust_&_support/bg_support.jpg);
        background-size: cover;
        background-position: 10%;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part {
        line-height: 55px;
    }

    .rs-services.style19 .services-item .services-wrap .services-content .services-title .title a:hover {
        color: #106eea;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part.purple-bg:before {
        border: 1px solid #106eea;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part.purple-bg {
        background: #106eea;
    }

    @media only screen and (max-width: 480px) {
        .sec-title .desc {
            display: block;
        }


    }
</style>
<!-- Internal-css-Ends -->

<body class="home-eight">
    <!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!--End preloader here -->
    <!--Full width header Start-->
    <div class="full-width-header header-style4">
        <!--header-->
        <?php include '../../header.php'; ?>
        <!--Header End-->
    </div>
    <!--Full width header End-->
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Section Start -->
        <div class="rs-breadcrumbs bg-3">
            <div class="container">
                <div class="content-part text-center">
                    <p><b>Services - Software Development</b></p>
                    <h1 class="breadcrumbs-title  mb-0">
                        Application Customization and Support</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs Section End -->
        <!-- 1st intro section starts -->
        <div class="rs-solutions pb-100 mt-100 md-pb-80">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 md-order-first md-mb-30">
                        <div class="image-part">
                            <img src="<?php echo main_url; ?>/assets/images/services/software-development/application_cust_&_support/suprt_main.jpg" alt="Application Customization and Support">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="sec-title mb-24">
                            <div class="desc mb-2">Application customization and support services help customers to ensure that applications are reliable, highly available, scalable and relevant to evolving business needs.</div>
                            <div class="desc mb-2">NetServ team has years of experience in design, software development, working with applications. Our software development team works on application customization and managing applications.</div>
                            <div class="desc mb-2">Speak to an experienced consultant to get started</div>
                        </div>
                        <!-- <div class="btn-part mt-42">
                            <a class="readon modify" href="#">Contact Us</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- 1st intro section ends  -->
        <!-- form starts  -->
        <div id="rs-contact" class="rs-contact style1 gray-bg pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
                <div class="white-bg">
                    <div class="row">
                        <div class="col-lg-8 form-part">
                            <div class="sec-title mb-35 md-mb-30">
                                <div class="sub-title primary">CONTACT US</div>
                                <h2 class="title mb-0">Get In Touch</h2>
                            </div>
                            <div id="form-messages"></div>
                            <form id="contact-form" class="contact-form" method="post" action="https://rstheme.com/products/html/reobiz/mailer.php">
                                <div class="row">
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="text" name="fname" placeholder="First Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="text" name="lname" placeholder="Last Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="email" name="email" placeholder="Email" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="text" name="phone" placeholder="Phone Number" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-30">
                                        <div class="common-control">
                                            <textarea name="message" placeholder="Your Message Here" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="submit-btn">
                                            <button type="submit" class="readon">Submit Now</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-4 pl-0 md-pl-pr-15 md-order-first">
                            <div class="contact-info">
                                <h3 class="title text-center" style="line-height: 44px;">What more? Our Application Customization and Support can offer you various other benefits to strengthen your application-customization-and-support. Want to try out?</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- form ends -->
    </div>
    <!-- Main content End -->
    <!-- Footer Start -->
    <?php include '../../footer.php'; ?>
    <!-- Footer End -->
    <!-- start scrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End scrollUp  -->
    <?php include '../../service_jslinks.php'; ?>
</body>

</html>