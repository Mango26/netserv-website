    <?php include_once("constants.php");?>
    <!-- favicon -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- favicon -->
    <meta property="og:title" content="NetServ - Simplify And Secure IT Infrastructure." />
    <meta property=”og:type” content=”website” />
    <meta property=”og:url” content=”https://ngnetserv.com” />
    <meta property="og:image" content="https://ngnetserv.com/assets/images/logo.png" />
    <meta name=”twitter:card” content=”NetServ Simplify And Secure IT Infrastructure.” />
    <meta name=”twitter:title” content=”NetServ - Simplify And Secure IT Infrastructure.” />
    <meta name=”twitter:description” content=”description here” />
    <meta name=”twitter:url” content=”https://ngnetserv.com” />
    <meta name=”twitter:image” content=”https://ngnetserv.com/assets/images/logo.png” />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo main_url;?>/assets/images/favicon.png">
    <!-- Bootstrap v4.4.1 css -->
    <!-- font-awesome css -->
    <!-- animate css -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo main_url;?>/assets/css/animate.css">
    <!-- aos css -->
    <link rel="stylesheet" type="text/css" media="all"  href="<?php echo main_url;?>/assets/css/aos.css">
    <!-- slick css -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo main_url;?>/assets/css/slick.css">
    <!-- off canvas css -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo main_url;?>/assets/css/off-canvas.css">
    <!-- linea-font css -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo main_url;?>/assets/fonts/linea-fonts.css">
    <!-- flaticon css  -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo main_url;?>/assets/fonts/flaticon.css">
    <!-- Main Menu css -->
    <link rel="stylesheet" media="all" href="<?php echo main_url;?>/assets/css/rsmenu-main.css">
    <link rel="stylesheet" media="all"  href="<?php echo main_url;?>/assets/css/rs-animations.css">
    <!-- rsmenu transitions css -->
    <link rel="stylesheet" media="all"  href="<?php echo main_url;?>/assets/css/rsmenu-transitions.css">
    <!-- spacing css -->
    <link rel="stylesheet" media="all" type="text/css" href="<?php echo main_url;?>/assets/css/rs-spacing.css">
    <!-- style css -->
    <link rel="stylesheet" media="all" type="text/css" href="<?php echo main_url;?>/style.css"> <!-- This stylesheet dynamically changed from style.less -->
    <!-- responsive css -->
    <link rel="stylesheet" media="all"  type="text/css" href="<?php echo main_url;?>/assets/css/responsive.css">
    <link rel="stylesheet" media="all" type="text/css" href="<?php echo main_url;?>/assets/css/custom.css">
    <link rel="stylesheet" media="all" type="text/css" href="<?php echo main_url;?>/assets/css/responsive_tablet.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
  <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5VL7HQH');</script>
    <!-- End Google Tag Manager -->
<script>
    var scripts = [
        "<?php echo main_url;?>/assets/css/bootstrap.min.css",
        "<?php echo main_url;?>/assets/css/font-awesome.min.css?display=swap",
    ];
    scripts.forEach((script) => {
        let tag = document.createElement("link");
        tag.rel  = 'stylesheet';
        tag.type = 'text/css';
        tag.media = 'all';
        tag.setAttribute("href", script);
        document.head.appendChild(tag);
    });
</script>
</head>
