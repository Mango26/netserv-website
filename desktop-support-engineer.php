<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Desktop Support Engineer</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/desktop-support-engineer " />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4 {
        background-image: url(assets/images/bg/bg4.png)
    }

    .rs-collaboration.style1 .img-part img {
        position: relative;
        bottom: 0
    }

    .rs-services.style22 .service-wrap .icon-part img {
        width: 53px;
        height: 53px;
        max-width: unset
    }

    ul.listing-style li {
        position: relative;
        padding-left: 30px;
        line-height: 34px;
        font-weight: 500;
        font-size: 14px
    }

    ul.listing-style.regular2 li {
        font-weight: 400;
        margin-bottom: 0
    }

    .rs-about.style10 .accordion .card .card-body {
        background: #fff
    }
</style>

<body class="home-eight"><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div class="full-width-header header-style4">
        <?php include 'header.php'; ?> </div>
    <div class="main-content">
        <div class="rs-pricing style1">
            <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
                <div class="container">
                    <div class="sec-title">
                        <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Desktop Support Engineer</h1>
                        <div class="sub-title text-center white-color"> Hercules, California (USA)</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-left">
                            <p> <span class="txt_clr"><strong>Designation</strong> : </span>Desktop Support Engineer</p>
                            <p> <span class="txt_clr"><strong>Experience</strong> : </span>5+ Years</p>
                            <p> <span class="txt_clr"><strong>Salary </strong> : </span>Best in Industry</p>
                            <p> <span class="txt_clr"><strong>Joining </strong> : </span>Immediate/15 days </p>
                            <p> <span class="txt_clr"><strong>Location </strong> : </span>Hercules, California (USA)(Onsite)</p>
                            <p> <span class="txt_clr"><strong>Job Description</strong></span></p>
                            <ol>
                                <li>Work independently in troubleshooting and providing solutions to unresolved hardware and software problems through trouble-ticket system</li>
                                <li>Configuration of network printers. </li>
                                <li>Provide a single point of contact for end users to receive support and maintenance within the organization's desktop computing environment. </li>
                                <li>Develops and manages effective professional working relationships with users, co-workers. </li>
                                <li>Hands on experience on: Installation, Configuration, Manage and support Office 365 and all types of Application Software in Windows as well as MAC operating systems. </li>
                                <li>IT asset management using ITSM tool BMC remedy</li>
                                <li>IT asset inventory management.</li>
                                <li>Set up desktop computers/Laptop and peripheral devices and test network connections
                                 <li>Create DHCP Scopes / IP Reservations etc.</li>
                                 <li>Assist and manage IT for migrations, deployment, build outs, trading floor offices wiring and cable management</li>
                                 <li>Taking care of L1 & L2 support for server along with Networking team. </li>
                                 <li>Creating a problem ticket accordingly to the incident was happened and helps them for troubleshooting. </li>
                                <li>Implement Policies on User and Computers in Windows Server 2003, Windows Server 2008, Windows server 2012, Windows server 2016. </li>
                                 <li>Perform and maintain regular Backup of Operating systems and perform restorations according to requirements. </li>
                                 <li>Resolve minor to moderate hardware, software, and network error issues & Verifies basic PC and network functionality. </li>
                                 <li>Perform activity of Migrating Windows server 2008 to Windows server 2012 with all Active Directory Users / computers, Group Policies, DHCP Scopes etc. </li>
                                 <li>Troubleshooting of Computers / Laptops / Printers / Scanners etc. </li>
                                 <li>Coordination with Vendors likes Hp, Dell and Lenovo etc. for Hardware related issues or installation of new machine or peripherals. </li>
                                 <li>Provide Smart Hand Support to customers/clients as per their requirement.</li>
                                <li>Making Rack inventory. </li>
                                 <li>Basic knowledge about Networking. </li>
                                <li>Ensure adherence to processes as per company defined SLA. </li>
                                 <li>Maintaining Network Devices Router, Switches and server along with networking team. </li>
                            </ol>
                            </p>
                        </div>
                        <div class="btn-part"> <a href="mailto:zmohammed@ngnetserv.com" class="btn btn-primary">Apply</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php'; ?>
    <div id="scrollUp"> <i class="fa fa-angle-up"></i></div>
    <?php include 'service_jslinks.php'; ?>
</body>

</html>